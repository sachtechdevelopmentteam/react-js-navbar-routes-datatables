import React, { Component } from 'react';
import logo from './logo.svg'
import {  Collapse,Navbar,NavbarToggler,NavbarBrand,Nav,NavItem,NavLink,UncontrolledDropdown,DropdownToggle,DropdownMenu,DropdownItem } from 'reactstrap';
import { BrowserRouter as Router, Route, Link,Switch } from 'react-router-dom'
import './App.css';


import Home from './home';
import About from './about';


const brandImage = {
  height:'60px',
  width:'60px',
}

class App extends Component {

  constructor(props) {
    super(props);
     this.toggle = this.toggle.bind(this);
     this.state = {
       isOpen: false
     };
   }

   toggle() {
     this.setState({
       isOpen: !this.state.isOpen
     });
   }
  render() {
    return (
    <Router>
    <div>
    <Navbar color="light" light expand="md">
      <NavbarBrand ><Link to="/home"><img src={logo} style={brandImage }/></Link>
      </NavbarBrand>
      <NavbarToggler onClick={this.toggle} />
      <Collapse isOpen={this.state.isOpen} navbar>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink ><Link to="/about">About</Link></NavLink>
          </NavItem>
          <NavItem>
            <NavLink > Contact Us</NavLink>
          </NavItem>
          <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
              Dropdown 
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                Option 1
              </DropdownItem>
              <DropdownItem>
                Option 2
              </DropdownItem>
              <DropdownItem divider />
              <DropdownItem>
                Reset
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </Collapse>
    </Navbar>



      <Switch>   
        <Route path="/home" component={Home} />
        <Route path="/about" component={About} />
        {/* <Route path="/" component={Home} /> */}
     
      </Switch>
  </div>
 </Router>

     );
  }
}

export default App;
