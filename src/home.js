import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';


class Home extends Component{
    constructor(props) {
        super(props);

        this.state = {
            columnDefs: [
                {headerName: 'Make', field: 'make'},
                {headerName: 'Model', field: 'model'},
                {headerName: 'Price', field: 'price',editable: true},
                {headerName: 'Price', field: 'price',editable: true},
                {headerName: 'Price', field: 'price',editable: true},
                {headerName: 'Price', field: 'price',editable: true},
                {headerName: 'Price', field: 'price',editable: true},
                {headerName: 'Price', field: 'price',editable: true},
            ],
            rowData: [
                {make: 'Toyota', model: 'Celica', price: 35000},
                {make: 'Ford', model: 'Mondeo', price: 32000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
                {make: 'Porsche', model: 'Boxter', price: 72000},
            ]
        }
    }
    render(){
        return(
    
        <div className="ag-theme-balham">

            <AgGridReact
                enableSorting={true}
                enableFilter={true}
                pagination={true}
                columnDefs={this.state.columnDefs}
                rowData={this.state.rowData}>
            </AgGridReact>
        </div>
            

        );

    }
}
export default Home;